package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class OpentapsCRMPage extends ProjectMethods{

	public OpentapsCRMPage typeCompanyname(String data) {
		WebElement eleCompanyname = locateElement("id", "updateLeadForm_companyName");
		type(eleCompanyname, data);
		return this;
	}
	
	
	public ViewLeadPage clickUpdate() {
		WebElement eleUpdate = locateElement("xpath", "//input[@value='Update']");
		click(eleUpdate);
		return new ViewLeadPage();
	}
	
}