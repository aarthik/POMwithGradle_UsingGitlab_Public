package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods{

	
	public OpentapsCRMPage clickEdit() {
		WebElement eleEditbutton = locateElement("linktext", "Edit");
		click(eleEditbutton);
		return new OpentapsCRMPage();
	}
	
	public MyLeadsPage clickDelete() {
		WebElement eleDeletebutton = locateElement("linktext", "Delete");
		click(eleDeletebutton);
		return new MyLeadsPage();
	}
		
		
}